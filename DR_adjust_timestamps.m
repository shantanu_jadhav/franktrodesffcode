function DR_adjust_timestamps(animaldir)
% go through all of the epochs of data for specified animal directory and adjust the timestamps in all relevant files to
% ensure that the epochs are sequential in time with 5 minute gaps between

% DR changed output adj timestamps type to double (float64) to support long recording sessions
% UPDATE:: sept 2016... you probably shouldn't use this version... DR fixed the original 32 bit adjust_timestamps bug
% so that the offset doesn't blow up.. so use that unless you have a very good reason to convert to double

suffix = '.adj';
epoch_interval = 5 * 60 * 30000; % five minute space between epoch
cwd = pwd;

cd(animaldir); %session dir

% get a list of the mda directories
mda_dir_list = dir('*.mda'); % one .mda dir / epoch

toffset = 0;
% go through each of the directories in turn
for enum = 1:length(mda_dir_list) %loop over epochs
    disp(sprintf('adjust_timestamp processing epoch %d', enum));
    tmp = strsplit(mda_dir_list(enum).name, '.');
    % get the basename for this set of directories so we can create the various names we need for different
    % datatypes
    basename{enum} = tmp{1};
    
    % STEP 1: get the times from the current epoch's mda format timestamp file
    if isdir(strcat(basename{enum}, '.mda'))
        cd(strcat(basename{enum}, '.mda')); %session/epoch
        % open the timestamps files
        timestamp_file = dir('*timestamps.mda'); %should only be one unadjusted timestamps file
        if (length(timestamp_file) > 1) %this shouldn't occur anymore...
            warning(sprintf('multiple timestamp files (%d) in %s, using %s', length(timestamp_file), pwd, timestamp_file(end).name));
        end
        disp(sprintf('Processing mda timestamp file %s', timestamp_file(end).name));
        timestamp = readmda(timestamp_file(end).name);
    else
        disp(sprintf('ERROR: no .mda dir detected for epoch #%d ... extract mda before proceeding',enum))
        return
    end
    
    % STEP 2: adjust the timestamps and rewrite the file with a suffix
    starttime = timestamp(1);
    timestamp = timestamp - starttime + toffset;
    endtime = timestamp(end);
    %newtsfile = (timestamp_file(end).name,suffix);
    newtsfile = newfilename(timestamp_file(end).name, suffix);
    % write the new adjusted timestamp file
    double(timestamp); %DR 9.5.16
    writemda(timestamp, newtsfile, 'float64'); %DR 9.5.16
%     writemda(timestamp, newtsfile, 'uint32');
    cd ..;
    
    % STEP 3: read in and adjust the LFP timestamps
    lfp_dir = dir(sprintf('%s*.LFP', basename{enum}')); %session / epoch
    if ~isempty(lfp_dir);
        cd(lfp_dir.name);
        % open the timestamp files
        lfptimestamp_file = dir('*timestamps.dat');  %should only be one unadjusted timestamps file
        if (length(lfptimestamp_file) > 1) %this checks for multiple unadjusted timestamp.dat files and uses the last one
            warning(sprintf('multiple timestamp files (%d) in %s, using %s', length(lfptimestamp_file), pwd, lfptimestamp_file(end).name));
        end
        adjlfptimestamp_file = dir('*timestamps.adj.dat');
        if ~isempty(adjlfptimestamp_file)
            disp(sprintf('adjusted LFP timestamps detected, overwriting for epoch#%d',enum))
        else
            disp(['Processing LFP timestamp file ' lfptimestamp_file(end).name]);
        end
            fileID = fopen(lfptimestamp_file(end).name, 'r');
            header = readtrodesexportheader(fileID);
            % read in the timestamps
            timestamp = fread(fileID, 'uint32');
            fclose(fileID);
            % adjust the timestamps
            double(timestamp); %DR 9.5.16
            timestamp = timestamp - starttime + toffset;
            % write out the new file
            newtsfile = newfilename(lfptimestamp_file(end).name, suffix);
            fileID = fopen(newtsfile, 'w');
            % write the header. Note that we're not updating the Time offset field at the moment
            writetrodesexportheader(fileID, header);
            % write the data
            fwrite(fileID, timestamp, 'double'); %DR 9.5.16
%             fwrite(fileID, timestamp, 'uint32');
            fclose(fileID);
            cd ..
%         end
    else
        disp(sprintf('no .LFP dir detected for epoch #%d',enum))
    end
    
    % Step 4: read in and adjust the position timestamps (note that there can be multiple position directories for a
    % given epoch
    pos_dir = dir(sprintf('%s*.pos', basename{enum}')); %session / epoch
    if ~isempty(pos_dir);
        cd(pos_dir.name); %session / epoch
        % open the timestamp files
        postimestamp_file = dir('*timestamps.dat');
        if (length(postimestamp_file) > 1) %this checks for multiple unadjusted timestamp.dat files and uses the last one
            warning(sprintf('multiple timestamp files (%d) in %s, using %s', length(postimestamp_file), pwd, postimestamp_file(end).name));
        end
        adjpostimestamp_file = dir('*timestamps.adj.day');
        if ~isempty(adjpostimestamp_file) % this checks for a timestamps.adj.dat file
            disp(sprintf('adjusted pos timestamps detected, overwriting for epoch#%d',enum))
        else
            disp(['Processing timestamp file ' postimestamp_file(end).name]);
        end
            fileID = fopen(postimestamp_file(end).name, 'r');
            header = readtrodesexportheader(fileID);
            % read in the timestamps
            timestamp = fread(fileID, 'uint32');
            fclose(fileID);
            % adjust the timestamps
            double(timestamp); %DR 9.5.16
            timestamp = timestamp - starttime + toffset;
            % write out the new file
            newtsfile = newfilename(postimestamp_file(end).name, suffix);
            fileID = fopen(newtsfile, 'w');
            % write the header. Note that we're not updating the Time offset field at the moment
            writetrodesexportheader(fileID, header);
            % write the data
            fwrite(fileID, timestamp, 'double'); %DR 9.5.16
%             fwrite(fileID, timestamp, 'uint32');
            fclose(fileID);
            cd ..
%         end
    else
        disp(sprintf('no .pos dir detected for epoch #%d',enum))
    end
    
    % STEP 5: read in and adjust the DIO timestamps
    dio_dir =dir(sprintf('%s*.DIO', basename{enum}')); %session / epoch
    if ~isempty(dio_dir);
        dispDIOoverwrite = 1;
        cd(strcat(basename{enum}, '.DIO'));
        % go through the files one by one
        dio_file = dir('*dio*.dat'); %there is a DIO for each of the DIO's in each epoch
        for dio_ind = 1:length(dio_file)
            %check to see if this is an already adjusted file; if so, report, skip it.. it will get over written
            if ~isempty(strfind(dio_file(dio_ind).name,'adj')) %this will skip all the already adj files
                if dispDIOoverwrite
                    disp(sprintf('adjusted DIO timestamps detected, overwriting for epoch#%d',enum))
                    dispDIOoverwrite = 0; %suppress further overwrite warnings for this epoch
                end
            else
                if dio_ind == 1
                    disp(['Processing dio files']);
                end
                fileID = fopen(dio_file(dio_ind).name, 'r');
                header = readtrodesexportheader(fileID);
                offset = ftell(fileID);
                % read in the timestamps and then the status
                timestamp = fread(fileID, Inf,'uint32', 1);
%                 timestamp = fread(fileID, Inf,'uint32'); %DR 9.5.16
                % adjust the timestamps
%                 double(timestamp); %DR 9.5.16
                timestamp = timestamp - starttime + toffset;
                % read in the state
                fseek(fileID, offset+4, 'bof');
                state = fread(fileID, Inf,'uint8', 4);
                fclose(fileID);
                % open up the new files
                newdiofile = newfilename(dio_file(dio_ind).name, suffix);
                fileID = fopen(newdiofile, 'w');
                writetrodesexportheader(fileID, header);
                % matlab doesn't allow writes of different datatypes, so we typecast everything to uint8, append and then write
%                 tsuint32 = cast(timestamp, 'uint32');
%                 tstmp = typecast(tsuint32, 'uint8');
                tstmp = timestamp'; %DR 9.5.16
%                 ntimestamps = length(timestamp);
%                 tstmp = reshape(tstmp, 4, ntimestamps);
%                 double(state); %DR 9.5.16
%                 tstmp(5,:) = state;
                tstmp(2,:) = state';
                fwrite(fileID, tstmp, 'double'); %DR 9.5.16
%                 fwrite(fileID, tstmp, 'uint8');
                fclose(fileID);
            end
        end
        cd ..
    else
        disp(sprintf('no .DIO dir detected for epoch #%d',enum))
    end
    % STEP 6: read in and adjust spike files
    spikes_dir =dir(sprintf('%s*.spikes', basename{enum}')); %session / epoch
    if ~isempty(spikes_dir);
        dispspikesoverwrite = 1;
        cd(strcat(basename{enum}, '.spikes'));
        % go through the files one by one
        spikes_file = dir('*spikes*.dat');
        for spikes_ind = 1:length(spikes_file)
            %check to see if this is an already adjusted file; if so, report, skip it, and overwrite
            if ~isempty(strfind(spikes_file(spikes_ind).name,'adj')) %this will skip all the already adj files
                if dispspikesoverwrite
                    disp(sprintf('adjusted spikes timestamps detected, overwriting for epoch#%d',enum))
                    dispspikesoverwrite = 0; %suppress further overwrite warnings for this epoch
                end
            else
                if spikes_ind == 1
                    disp(['Processing spike files'])
                end
                fileID = fopen(spikes_file(spikes_ind).name, 'r');
                header = readtrodesexportheader(fileID);
                offset = ftell(fileID);
                % read in the timestamps and then the status
                timestamp = fread(fileID, Inf,'uint32', 160 * 2); % skip 160 points, each an int16
                % adjust the timestamps
%                 double(timestamp);
                timestamp = timestamp - starttime + toffset;
                % read in the waveforms
                fseek(fileID, offset+4, 'bof');
                spikewf = fread(fileID, Inf,'160*int16', 4);
                fclose(fileID);
                newspikesfile = newfilename(spikes_file(spikes_ind).name, suffix);
                % matlab doesn't allow writes of different datatypes, so we typecast everything to uint8, append and then write
%                 tsuint32 = cast(timestamp, 'uint32');
%                 tstmp = typecast(tsuint32, 'uint8');
                tstmp = timestamp'; %DR 9.5.16
                ntimestamps = length(timestamp);
%                 tstmp = reshape(tstmp, 4, ntimestamps);
%                 spikewfint16 = cast(spikewf, 'uint16');
%                 spikewftmp = typecast(spikewfint16, 'uint8');
                spikewftmp  = spikewf; %DR 9.5.16
                spike_extract_failed = 0;
                try
%                     spikewftmp = reshape(spikewftmp, 320, ntimestamps);
                    spikewftmp2 = reshape(spikewftmp, 160, ntimestamps);
                catch
                    spike_extract_failed = 1;
                    warning(sprintf('spikes file (%s) failed reshape, probably is not a tetrode. adjust_timestamps currently requires 4 channels/ntrode, skipping.', spikes_file(spikes_ind).name));
                end
                
                if ~spike_extract_failed
                    % open up the new files
                    fileID = fopen(newspikesfile, 'w');
                    writetrodesexportheader(fileID, header);
%                     tstmp(5:324,:) = spikewftmp;
                    tstmp = [tstmp; spikewftmp2]; %DR 9.5.16
                    fwrite(fileID, tstmp, 'double'); %DR 9.5.16
%                     fwrite(fileID, tstmp, 'uint8');
                    fclose(fileID);
                end
            end
        end
        cd ..
    else
        disp(sprintf('no .spikes dir detected for epoch #%d',enum))
    end
toffset = toffset + (timestamps(end) - timestamps(1)) + epoch_interval    
end
end

function newname = newfilename(name, suffix)
% put in the suffix before the final .dat or .mda
% parse the filename into
parts = strsplit(name, '.');
newname = parts{1};
for i = 2:(length(parts)-1)
    newname = strcat(newname, '.', parts{i});
end
% add in the suffix
newname = strcat(newname, suffix, '.', parts{end});
end


