% July 2016 == EXTRACT AND PRE-PROCESS ===
% _____________________________________________
%
% BinaryFiles ---> Frank Framework Files
% _____________________________________________

%___ TO DO ____________in order of priority____
% >>>make this into a function instead of a script
% >>>get MS clustering output into .spikes structs
% >>>truncate epoch ends outside of 'experiment' time
% >option to stitch epochs
% >debug code to check the created files
% >fix output saving of corrupted data check

%  ________________ SET PARAMS ____________________

function Trodes_dayprocess(animal, date, sessionNum, varargin);

%% Use your own version of runTrodes_dayprocess from the repo to call Trodes_dayprocess

adjusttimestamps = 0; %Run this once before processing anything else..
makeFFLFP = 0;
makereferencedFFLFP = 0; useconfig = 0;  % get ref info from config (currently isn't saved in extracted data)
makeFFDIO = 0;
makeFFPOS = 0;
makeFFLINPOS = 0;
makeFFspikes = 0;
updatetaskstruct = 0; %create and/or update taskinfo
updatetetcellstructs = 0; % create and/or update tetinfo and cellinfo structs
makeRippleFilteredLFP = 0;
extractRipples = 0; %old method ripple detection
extractRipplesKonsensus = 0; % new consensus method..KK has other options that alter smooth,square,mean order... see KK's extraction.m
% ___________________ tmpPARAMS ___________________
cmperpix = []; % e.g. [0.1] eventually just grab this from .trodescomments
posSource = '';
taskcoordinateprogram = [ {'wtrack'} {'getcoord_wtrack'}; {'sixarmtrack'} {'getcoord_6armtrack'}]; %'sj_getcoord_wtrack' for W track..'doc' getcoord_wtrack for click order
nTrodeareas = []; % [ {area1} {[tetrodes]} ; {area2} {tetrodes} ... ] e.g. [{'ca1'} {[1, 16:30]} ; {'mec'} {[3:8 11:14]} ; {'por'} {[10 15]} ; {'v2l'} {[9]}; {'ref'} {[2 18]}];
refNTrodes = [];
RIPmindur = 0.015; % time (in seconds) which the signal must remain above threshold to be counted as as ripple (start with 0.015)
RIPnstd = 2; % > # std ripples (start with 2)
OLDRIPtetrodes = []; % -1 to specify all tetrodes. e.g. [1 16 17 19:30] .. eventually grab this from tetinfo struct based on area
consensusRIParea = ''; %define source region of ripples.. saved structure will contain this string e.g. 'por'
useNearestNeighbor = 0;
epochtypesordered = '';
epochtagsordered = '';
overwriteTrackNodes = 0;
% ___________________ Debugging ___________________
checkforcorrupteddata = 0;
checknumCommentfiles = 0; %not sure if this is necessary anymore.. not using comments files for anything..yet
% ______________ process varargin and overwrite default values _________________
if (~isempty(varargin))
    assign(varargin{:});
end
if isempty(animal) || isempty(date) || isempty(sessionNum)
    disp('you need to define animal, date, and sessionNum at the very least..try again')
    return
end
%% Get animal info, create directories if needed
% animaldef should now contain (in this order): name, filterframework dir, prefix/name, raw dir, preprocessing dir
animalinfo = animaldef(lower(animal));
animalID = animalinfo{1,3}; %use anim prefix for name
FFanimdir =  sprintf('%s',animalinfo{1,2}); %this is made below
rawdaydir = sprintf('%s%d/',animalinfo{1,4}, date); %this was manually made as a repository for .rec files, etc
preprocdaydir =  sprintf('%s%d/',animalinfo{1,5}, date); %this was made through python trodes to binary extraction
if ~isdir(FFanimdir); mkdir(FFanimdir); end
if ~isdir([FFanimdir 'EEG']); mkdir([FFanimdir 'EEG']); end
try
    cd(preprocdaydir)
catch
    mkdir(preprocdaydir)
    cd(preprocdaydir)
end
%% check the log files of the binaries for corrupted data
if checkforcorrupteddata
    cd(preprocdaydir)
    cd ..
    %direct call to the system.. but there's no way to save the output
        !grep -H 'Warning' ./*/*/*.log 
    [~,warninglog] = system('grep -H "Warning" ./*/*/*.log')% output currently gets saved as 1D char string.. need to fix this
end
%% check that the # of comments files matches the # of .recs (need to test this)
if checknumCommentfiles
    cd(rawdaydir)
    numCommentsFiles = length(dir('*.trodesComments'));
    numRecFiles = length(dir('*.rec'));
    if numCommentsFiles ~= numRecFiles
        error('uneven number of .trodesComments and .rec files');
    end
end
%% ADJUST TIMESTAMPS
if adjusttimestamps
%     DR_adjust_timestamps(preprocdaydir);
    adjust_timestamps(preprocdaydir);
end
%% create Frank Framework files
if makeFFLFP   %  ** this makes UNREFERENCED lfp files (called eeggnd).
    disp('starting create FFv1 LFP files'); Binary2FF_LFP(animalID, preprocdaydir, FFanimdir, sessionNum, nTrodeareas, refNTrodes); disp('FFv1 LFP files created')
end
if makereferencedFFLFP  %  ** this REFERENCES the eeg according to config file or refchan specified in the eeggnd file
    if useconfig; disp('using trodes config info to reference'); end
    disp('referencing LFP files'); FF_LFPreferencer(animalID, rawdaydir, FFanimdir, sessionNum,'useconfig',useconfig); disp('LFP referencing complete')
end
if makeFFDIO
    disp('starting create FFv1 DIO files'); Binary2FF_DIO(animalID, preprocdaydir, FFanimdir, sessionNum); disp('FFv1 DIO files created')
end
if makeFFPOS
    if isempty(cmperpix); disp('cmperpix is empty.. aborting'); return; end
    disp('starting create FFv1 POS files'); Binary2FF_POS(animalID, preprocdaydir, FFanimdir, sessionNum, date, 'cmperpix', cmperpix, 'posSource', posSource, 'useNearestNeighbor', useNearestNeighbor); disp('FFv1 POS files created')
end
%% CREATE INFO STRUCTURES
if updatetaskstruct % TASK INFO STRUCT
    if ~isempty(epochtypesordered)
        cd(FFanimdir);
            disp('creating taskstruct files')
%             epochtypes = unique(epochtypesordered,'stable');
            for t = 1:length(epochtypesordered)
                dayepochlist = [sessionNum t];
                if strncmp(epochtypesordered{t},'wtrack', length('wtrack'))
                    coordprogramInd = find(strcmp(taskcoordinateprogram(:,1),'wtrack'));
                    disp(sprintf('using %s for the taskcoordinateprogram', taskcoordinateprogram{coordprogramInd, 2}));
                    createtaskstruct(FFanimdir, animalID, dayepochlist, taskcoordinateprogram{coordprogramInd, 2}, 'overwrite',overwriteTrackNodes);
                elseif strncmp(epochtypesordered{t},'sixarmtrack', length('sixarmtrack'))
                    coordprogramInd = find(strcmp(taskcoordinateprogram(:,1),'sixarmtrack'));
                    disp(sprintf('using %s for the taskcoordinateprogram', taskcoordinateprogram{coordprogramInd, 2}));
                    createtaskstruct(FFanimdir, animalID, dayepochlist, taskcoordinateprogram{coordprogramInd, 2}, 'overwrite',overwriteTrackNodes, 'usevidframe', 1, 'cmperpix', cmperpix);
                end
            end

        for t = 1:length(epochtypesordered)
            if strcmp(epochtypesordered{t},'sleep')
                epochtags = {'environment', 'sleep', 'type', 'sleep'};
            else
                epochtags = {'environment', epochtypesordered{t}, 'type', 'run'};
            end
            DR_addtaskinfo(FFanimdir, animalID, sessionNum, t, epochtags); % DR version to be able to take cell array epochtags
        end
    else
        error('epochtypesordered required')
    end
end
if updatetetcellstructs % TET AND CELL INFO STRUCTS.. if struct already exists, it will append to existing
    if isempty(nTrodeareas); disp('nTrodeareas is empty!! aborting'); return; end
    cd(FFanimdir);
    if ~isempty(dir([animalID, 'tetinfo' num2str(sprintf('%02d', sessionNum)) '.mat'])) % Look for existing struct
        disp('tetinfo struct file detected.. will try to append to existing structure')
    else
        createtetinfostruct(FFanimdir, animalID);
    end
    if ~isempty(dir([animalID, 'cellinfo' num2str(sprintf('%02d', sessionNum)) '.mat'])) % Look for existing struct
        disp('cellinfo struct file detected.. will try to append to existing structure')
    else
        createcellinfostruct(FFanimdir, animalID);
    end
    for i = 1:length(nTrodeareas(:,1)) %this updates both tet and cell info structs with tet locations across all epochs
        DR_addtetrodelocation(FFanimdir, animalID,nTrodeareas{i,2},nTrodeareas{i,1}); %DR version to continue through absent cell info
    end
end
%% CREATE LINPOS FILES
if makeFFLINPOS
    if exist('dr_lineardayprocess')
        dr_lineardayprocess(FFanimdir, animalID, sessionNum); %DR's version based on KK's version
    else
        lineardayprocess(FFanimdir, animalID, sessionNum); %untested by dr.. but supposedly was original version
    end
end
%% CREATE RIPPLE-BAND LFP FILES
if makeRippleFilteredLFP
    DR_rippledayprocess(FFanimdir, animalID, sessionNum)
end
%% EXTRACT RIPPLES 'OLD' METHOD .. add progress outputs for this.. takes awhile
if extractRipples
    if isempty(OLDRIPtetrodes) || isempty(epochtags); disp('epochtags and epochs2tag are empty!! aborting'); return; end
    disp(sprintf('using %d RIPmindur and %d RIPnstd ',RIPmindur,RIPnstd))
    DR_extractripples(FFanimdir, animalID, sessionNum, OLDRIPtetrodes, RIPmindur, RIPnstd);
end
%% EXTRACT RIPPLES CONSENSUS METHOD
if extractRipplesKonsensus
    for riparea = 1:length(consensusRIParea);
        conRIPtetfilters = {sprintf('(isequal($area, ''%s''))', consensusRIParea{riparea})}; %define source region of ripples
        if isempty(consensusRIParea{riparea}) || isempty(conRIPtetfilters); disp('consensusRIParea and conRIPtetfilters are empty!! aborting');
            return;
        end
        DR_extractkonsensus(FFanimdir, animalID, sprintf('%sripples',consensusRIParea{riparea}), 'ripple',sessionNum,conRIPtetfilters,RIPmindur,RIPnstd) %looks like this version individually squares, smooths, sqroots, then means traces
    end
end
%% CONVERT MOUNTAINSORT OUTPUT INTO .SPIKES  skeleton code.. needs to be finished/tested
if makeFFspikes
    %check for exiting spike struct for today
    if isdir(sprintf('%s/%sspikes%s',FFanimdir,animalID,sessionNum))
        load(sprintf('%s/%sspikes%s',FFanimdir,animalID,sessionNum))
    end
    %     MSserverdaydir = FFanimdir =  sprintf('%s/%s',animalinfo{1,4}, date);
    cd(FFanimdir)
    dayeps = dir('*.mountain*');
    numepochs = length(dayeps.name)
    for epoch = numepochs
        epntrode = dir('*.ntrode*')
        for ntrode = epntrode
            %             cd(ntrode)
            spikes{sessionNum}{epoch}{ntrode} = mda_to_spikes(firingsmda,annotationmda,timestampsmda)
        end
    end
    save(sprintf('%s/%sspikes%s',FFanimdir,animalID,sessionNum),spikes)
end
%%
clear all
end






















