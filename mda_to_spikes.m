%MDA to spike struture; JC 20160703
%Creates a cell for a given tetrode, with all cluster structs contained

%Requires: readmda.m, varargin2struct.m, loadjson.m, jsonopt.m,
%mergestruct.m, struct2jdata.m

%Inputs: 
%firingsmda --> the raw firings.mda from mountainsort
%annotationmda --> the annotations exported from mountainview
%timestampsmda --> the ADJUSTED timestamps.mda from previous steps

%Output:
%A single tetrode's spikes, containing cells for each cluster on this tetrode

function tetrode=mda_to_spikes(firingsmda,annotationmda,mountainviewjson, timestampsmda)
tetrode={};

real_timestamps = readmda(timestampsmda);
ms_annotated = readmda(annotationmda);
mv_params = loadjson(mountainviewjson);
ms_clusters = readmda(firingsmda);

ms_labels = ms_clusters(3,:); %labels row
ms_times = ms_clusters(2,:); %times row

%Filtering based on .mv json settings
if mv_params.event_filter.use_event_filter
    disp('Using event filter');
    ms_detectability = ms_clusters(6,:); %Hardcoded due to format of firings.mda
    ms_outlier = ms_clusters(5,:); %Hardcoded due to format of firings.mda
    event_filter = (ms_detectability > mv_params.event_filter.min_detectability_score) & (ms_outlier < mv_params.event_filter.max_outlier_score);
    ms_labels = ms_labels(event_filter);
    ms_times = ms_times(event_filter);
    if length(ms_labels) ~= length(ms_times)
        disp('filtered labels and times do not match');
        return
    end
end

clusters=ms_annotated(1,:); %all clusters row

% Reduce to accepted clusters
accepted_clust=clusters(logical(ms_annotated(2,:)));

%Create list of labels that will ultimately make it into the spike structure
final_cluster_labels=accepted_clust(ismember(accepted_clust,ms_annotated(3,:)));

% Reduce to clusters that need reassignment
clust_to_merge=accepted_clust(not(ismember(accepted_clust,ms_annotated(3,:))));
clust_to_merge_newlabel = ms_annotated(3,clust_to_merge);

ms_labels_merged=ms_labels;

for ii=1:length(clust_to_merge)
    %Reassign labels
    ms_labels_merged(ismember(ms_labels,clust_to_merge(ii)))=clust_to_merge_newlabel(ii);
end

%reduce to accepted times only
ms_times = ms_times(ismember(ms_labels_merged,accepted_clust));
ms_labels_merged = ms_labels_merged(ismember(ms_labels_merged,accepted_clust));

%Now we need to grab all the times for specific clusters
for ii=1:length(final_cluster_labels)
    %Get times for this cluster
    clust_times = ms_times(ismember(ms_labels_merged,final_cluster_labels(ii)));%times for all events matching cluster label
    clust_times_seconds=real_timestamps(clust_times)/30000;%PYTHON will need to -1 the indices relative to this
    %Pre-allocate data with NaN
    data=NaN(length(clust_times),10); %Missing posindex, and all other data
    
    data(:,1)=clust_times_seconds; %Put times into data
    %Construct cell struct, filled with NaN where data not available
    tetrode{final_cluster_labels(ii)}=struct('data',data,'descript','spike data','fields','time x y dir not_used amplitude(highest variance channel) posindex x-sm y-sm dir-sm', 'depth', NaN, 'spikewidth', NaN, 'timerange', [min(clust_times) max(clust_times)],'tag',NaN);
end
end