function Binary2FF_LFP(animalID, srcdir, destdir, sessionNum, nTrodeareas, refNTrodes)

%Demetris Roumis July 2016
%This function saves LFP data in the Filter Framework v1 format.
%It is assumed that there is a directory of binary files in the form of:
%<animalname>/preprocessed/<date (%YYYY%MM%DD)>/<date>_<animal>_<epoch>.LFP/...
%...<date>_<animal>_<epoch>.LFP_nt<ntrode>ch<channel>.dat
%ex: /mnt/data19/droumis/D10/preprocessed/20160512/20160512_D10_01.LFP/20160512_D10_01.LFP_nt4ch1.dat

%animalID -- a string identifying the animal's id (appended to the beginning of the files).. ex: 'D10'
%sessionNum -- day.. ex: '4'
%srcdir -- preprocessed binary directory.. ex: /mnt/data19/droumis/D10/preprocessed/20160512/
%destdir -- the directory where the processed files should be saved for the animal...
%ex: /mnt/data19/droumis/D10/filterframework/EEG/

cd(srcdir);
lfpepochDirs = dir('*.LFP');
lfpepochDirs = arrayfun(@(x) x.name,lfpepochDirs,'UniformOutput', false);
lfpepochDirs = sort(lfpepochDirs); %this should be safe if the file naming is consistent with '<date>_<animal>_<2digitepoch>.LFP'
disp('Processing LFP files...');
tic
for lfpepoch = 1:length(lfpepochDirs)
    cd(srcdir); %necessary for every loop after the first
    cd(lfpepochDirs{lfpepoch}); %go into current epoch lfp dir
    lfpntrodefiles = dir('*.LFP*');
    try
        timestampsfile = dir('*.timestamps.adj*');
    catch
        disp('no adjusted timestamps file detected.. hit any key to continue with unadjusted timestamps or ctrl-c to abort')
        pause
        timestampsfile = dir('*.timestamps*');
    end
    timestampsData = readTrodesExtractedDataFile(timestampsfile.name);
    tmpData = [];
    for lfpntrodeNum = 1:length(lfpntrodefiles) %load all ntrodes at once to be able to sort by ntrodeID
        tmpData{lfpntrodeNum} = readTrodesExtractedDataFile(lfpntrodefiles(lfpntrodeNum).name);
    end
    tmpid = cell2mat(cellfun(@(x) x.ntrode_id,tmpData,'UniformOutput', false)); %get the reorder indices
    [sortedIDs, tmpid]  = sort(tmpid);
    tmpData = tmpData(tmpid);
    for lfpntrodeNum = 1:length(sortedIDs)
        lfpntrodeName = sortedIDs(lfpntrodeNum);
        eeggnd = [];
        sampstep = tmpData{lfpntrodeNum}.clockrate / 1500; %get the integer sample step size. replace 1500 with the export arg once mattias implements
        timestamps_normalized = timestampsData.fields.data / sampstep;
        timestamps_normalized_Ids = round((timestamps_normalized - timestamps_normalized(1))+1);% 
        currdata = nan(max(timestamps_normalized_Ids),1); % nanitialize (copywrite demetris 2016)
        currdata(timestamps_normalized_Ids) = tmpData{lfpntrodeNum}.fields.data;
        currdata = double(currdata).*tmpData{lfpntrodeNum}.voltage_scaling; %get data into double type and voltage
        eeggnd{sessionNum}{lfpepoch}{lfpntrodeName}.data = currdata;
%         eeggnd{sessionNum}{lfpepoch}{lfpntrode}.data = double(tmpData{lfpntrode}.fields.data).*tmpData{lfpntrode}.voltage_scaling; %get data into double type and voltage
        eeggnd{sessionNum}{lfpepoch}{lfpntrodeName}.descript = tmpData{lfpntrodeNum}.description;
        eeggnd{sessionNum}{lfpepoch}{lfpntrodeName}.samprate = 1500; %mattias will implement this as an export arg in the future
        eeggnd{sessionNum}{lfpepoch}{lfpntrodeName}.clockrate = tmpData{lfpntrodeNum}.clockrate;
        eeggnd{sessionNum}{lfpepoch}{lfpntrodeName}.starttime = timestampsData.fields.data(1)/tmpData{lfpntrodeNum}.clockrate; %convert to seconds
        eeggnd{sessionNum}{lfpepoch}{lfpntrodeName}.endtime = timestampsData.fields.data(end)/tmpData{lfpntrodeNum}.clockrate; %convert to seconds
        eeggnd{sessionNum}{lfpepoch}{lfpntrodeName}.data_voltage_scaled = 1;
        %look for area and area ref ntrodes in the tetsareas input
        ntrodeindcell = cellfun(@(x) x==lfpntrodeName, nTrodeareas(:,2), 'UniformOutput', false);
        ntrodeind = cell2mat(cellfun(@(x) any(x), ntrodeindcell, 'UniformOutput', false));
        if any(ntrodeind)
            nTrodeArea = nTrodeareas(ntrodeind,1);
            eeggnd{sessionNum}{lfpepoch}{lfpntrodeName}.area = nTrodeArea;
            refntrodeindcell = cell2mat(cellfun(@(x) strcmp(x,(nTrodeareas{ntrodeind,1})), refNTrodes(:,1), 'UniformOutput', false));
            if any(refntrodeindcell)
                refNTrode = refNTrodes(refntrodeindcell,2);
                eeggnd{sessionNum}{lfpepoch}{lfpntrodeName}.refNTrode = refNTrode{1};
            end
        else %if it's not in the list of ntrodes, list itself as its reference
            eeggnd{sessionNum}{lfpepoch}{lfpntrodeName}.area = 'ref';
            eeggnd{sessionNum}{lfpepoch}{lfpntrodeName}.refNTrode = lfpntrodeName;
        end

        %         eeg{sessionNum}{lfpepoch}{lfpntrode}.trodes_datatype = tmpData{lfpntrode}.fields.type; %int16 in trodes
        eeggnd{sessionNum}{lfpepoch}{lfpntrodeName}.nTrode = tmpData{lfpntrodeNum}.ntrode_id;
        eeggnd{sessionNum}{lfpepoch}{lfpntrodeName}.nTrodeChannel = tmpData{lfpntrodeNum}.ntrode_channel;  %1-based
        if strcmp(tmpData{lfpntrodeNum}.reference, 'on');  % we export unref'd data, so this should never be the case
            eeggnd{sessionNum}{lfpepoch}{lfpntrodeName}.referenced = 1;
        else
            eeggnd{sessionNum}{lfpepoch}{lfpntrodeName}.referenced = 0;
        end
        eeggnd{sessionNum}{lfpepoch}{lfpntrodeName}.low_pass_filter = tmpData{lfpntrodeNum}.low_pass_filter;
        eeggnd{sessionNum}{lfpepoch}{lfpntrodeName}.voltage_scaling = tmpData{lfpntrodeNum}.voltage_scaling;
        %eeggnd{sessionNum}{lfpepoch}{lfpntrode}.refNtrode_chan = tmpData{lfpNtrode}.somethingTBD coming soon
        %one lfp file per epoch & ntrode
        save([destdir '/EEG/' animalID,'eeggnd',num2str(sprintf('%02d',sessionNum)),'-',num2str(lfpepoch),'-',num2str(sprintf('%02d',lfpntrodeName)),'.mat'],'-v6','eeggnd');
    end
end
FFv1filetime = toc; disp([sprintf('%.02f',FFv1filetime) ' seconds to make LFP files'])