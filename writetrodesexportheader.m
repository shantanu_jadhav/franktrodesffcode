%function header = writetrodesexportheader(filehandle, header)
%writes the header (a cell array of strings) and leaves the file pointer at the end of it
function header = writetrodesexportheader(filehandle, header)
    for ind = 1:length(header)
	fprintf(filehandle, '%s\n', header{ind});
    end
end


