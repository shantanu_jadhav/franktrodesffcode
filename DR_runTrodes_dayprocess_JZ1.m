

%Send parameters to Trodes_dayprocess
%Create one of these scripts per animal and archive with data

%-------------Animal Info-------------------
animal = 'JZ1';
% dates = [20161114 20161115 20161116 20161117 20161118 20161119 20161121 20161122 20161125 20161128 20161129 20161130 20161201 20161202]; % input a single day/session by it's 8 digit date YYYYMMDD e.g. 20160512
% sessionNums = [1 2 3 4 5 6 7 8 9 10 11 12 13 14]; % Day(s)
% dates = [20161115];
% sessionNums = [2]; % Day(s)
% dates = [20161128 20161129 20161130 20161201 20161202];
% sessionNums = [10 11 12 13 14]; % Day(s)
dates = [20161116 20161117 20161118 20161119 20161121 20161122 20161125 20161128 20161129 20161130 20161201 20161202]; % input a single day/session by it's 8 digit date YYYYMMDD e.g. 20160512
sessionNums = [3 4 5 6 7 8 9 10 11 12 13 14]; % Day(s)
sleepcmperpix = [0.06122];
runcmperpix = [0.06545];
nTrodeareas = [{'ca1'} {[16:30]} ; {'mec'} {[1:4 6:10 14]} ; {'por'} {[13 15]} ; {'sub'} {[5]} ; {'v2l'} {[12]}]; % [ {area1} {[tetrodes]} ; {area2} {tetrodes} ... ]
refNTrodes = [{'ca1'} {[11]} ; {'mec'} {[11]} ; {'por'} {[11]} ; {'ca1'} {[11]} ; {'sub'} {[11]} ; {'v2l'} {[11]}]; %gets used to reference LFP if useconfig == 0;
% octrodes = [3 10]; %just putting this here for now.. [probably can just inherit attribute from the spike struct if i ever need to know num of chans per ntrode

%List out the epoch types ordered for each day
epochtypesorderedCombined{1} = {'sleep', 'wtrack', 'sleep', 'wtrack', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{2} = {'sleep', 'wtrack', 'sleep', 'wtrack', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{3} = {'sleep', 'wtrack', 'sleep', 'wtrack', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{4} = {'sleep', 'wtrack', 'sleep', 'wtrack', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{5} = {'sleep', 'wtrack', 'sleep', 'wtrack', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{6} = {'sleep', 'wtrack', 'sleep', 'wtrack', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{7} = {'sleep', 'wtrack', 'sleep', 'wtrackrotated', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{8} = {'sleep', 'wtrack', 'sleep', 'wtrackrotated', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{9} = {'sleep', 'wtrack', 'sleep', 'wtrackrotated', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{10} = {'sleep', 'sixarmtrack_right', 'sleep', 'sixarmtrack_right', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{11} = {'sleep', 'sixarmtrack_right', 'sleep', 'sixarmtrack_right', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{12} = {'sleep', 'sixarmtrack_right', 'sleep', 'sixarmtrack_right', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{13} = {'sleep', 'sixarmtrack_right', 'sleep', 'sixarmtrack_right', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{14} = {'sleep', 'sixarmtrack_right', 'sleep', 'sixarmtrack_right', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{15} = {'sleep', 'sixarmtrack_right', 'sleep', 'sixarmtrack_right', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};

%-------------Params------------------
batch1 = 0;
batch2 = 0;
batch3 = 1;
adjusttimestamps = batch1; % step 1 ;  %Run this once before processing anything else..
makeFFLFP = batch1; % step 2
makereferencedFFLFP = batch1; useconfig = 0;  %NOTE: if your refs don't change across days, just use the refNtrodes attribute and set this to zero. DR: for D13.. set useconfig = 0.. 
makeFFDIO = batch1; % step 4;
makeFFPOS = batch1; % step 5;
makeRippleFilteredLFP = batch1; % step 6
updatetaskstruct = batch2; % step 7. requires user input for W track nodes
updatetetcellstructs = batch3; % step 8
makeFFLINPOS = batch3; % step 9
extractRipplesKonsensus = batch3; % step 10; new consensus method..KK has other options that alter smooth,square,mean order... see KK's extraction.m

%_____Don't Run these yet:________
extractRipples = 0; %old method ripple detection. DONT RUN THIS
makeFFspikes = 0; % code isn't done yet. DONT RUN THIS

% ___________________General Params (probably won't need to change these)___________________
% posSource = 'manual1';
posSource = 'online';
taskcoordinateprogram = [ {'wtrack'} {'getcoord_wtrack'}; {'sixarmtrack'} {'getcoord_6armtrack'}]; %'sj_getcoord_wtrack' for W track..'doc' getcoord_wtrack for click order
overwriteTrackNodes = 0; % set this to 1 if you want to overwrite the user specified nodes of the wtrack.. i.e. reruns createtaskstruct 
RIPmindur = 0.015; % time (in seconds) which the signal must remain above threshold to be counted as as ripple (start with 0.015)
RIPnstd = 2; % > # std ripples (start with 2)
OLDRIPtetrodes = [1 16 17 19:30]; % -1 to specify all tetrodes. eventually grab this from tetinfo struct based on area
consensusRIParea = {'ca1', 'mec', 'por'}; %define source region of ripples.. saved structure will contain this string
useNearestNeighbor = 1; %NN method current DR default.. AG set this to 0 for your data

% ___________________ Debugging ___________________
checkforcorrupteddata = 0;
checknumCommentfiles = 0; %not necessary anymore.. we aren't using the coments files


%% run Trodes_dayprocess with the vars above.. loop over dates/sessionNums
tic
if length(sessionNums) ~= length(dates); error('length of sessionNums must equal length of dates'); end
for i = 1:length(sessionNums);
    date = dates(i);
    sessionNum = sessionNums(i);
    epochtypesordered = epochtypesorderedCombined{sessionNum};
    cmperpix = repmat([sleepcmperpix runcmperpix],1,ceil(length(epochtypesordered)/2));
    cmperpix = cmperpix(1:length(epochtypesordered));
    
    %run dayprocess
    Trodes_dayprocess(animal, date, sessionNum, 'adjusttimestamps',adjusttimestamps, 'makeFFLFP', makeFFLFP,...
        'makereferencedFFLFP',makereferencedFFLFP, 'useconfig', useconfig, 'makeFFDIO', makeFFDIO, 'makeFFPOS', makeFFPOS, 'makeFFLINPOS', makeFFLINPOS, ...
        'makeFFspikes', makeFFspikes, 'updatetaskstruct', updatetaskstruct, 'updatetetcellstructs', updatetetcellstructs, ...
        'makeRippleFilteredLFP', makeRippleFilteredLFP, 'extractRipples', extractRipples, 'extractRipplesKonsensus', extractRipplesKonsensus, ...
        'cmperpix', cmperpix, 'posSource', posSource, 'taskcoordinateprogram', taskcoordinateprogram, ...
        'nTrodeareas', nTrodeareas, 'RIPmindur', RIPmindur, 'RIPnstd', RIPnstd, 'OLDRIPtetrodes', OLDRIPtetrodes, ...
        'consensusRIParea', consensusRIParea, 'checkforcorrupteddata', checkforcorrupteddata, 'checknumCommentfiles', checknumCommentfiles,...
        'useNearestNeighbor',useNearestNeighbor,'epochtypesordered', epochtypesordered, 'overwriteTrackNodes', overwriteTrackNodes,...
        'refNTrodes', refNTrodes);
end

timerTrodes_dayprocess = toc;
disp(sprintf('nextcoffeebreak = %0.0f seconds',timerTrodes_dayprocess));