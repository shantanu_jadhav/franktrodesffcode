function DR_extractkonsensus(animaldir, animalprefix, eventname, eventeegname, days, tetfilters, min_suprathresh_duration, nstd, varargin)

%function extractconsensus(directoryname, fileprefix, eventname, day, tetrode, min_suprathresh_duration, nstd, varargin)
%
%   Alternative approach to extractripples.  kk August 2014
%       Whereas extractripples + getriptimes extracts events from single tetrodes
%       and then subsequently constructs nrip vector, extractconsensus combines
%       event-band power across all valid detecting tetrodes then detects threshold SD events.

%       Follows Csicsvari--Buzsaki-1999 except instead of summing squared amplitudes
%       in "1.6 ms" windows, here keeps the original sampling rate (1500 Hz) and instead
%       Gaussian kernel smooths the combined power

% Aftewards, saves <animalprefix><event>con<day>.mat files in
% animdirect under {day}{epoch}

%animaldir - example '/data99/user/animaldatafolder/', a folder
%                containing processed matlab data for the animal
%
%fileprefix	- folder name where the day's data is stored
%
%day		- the day to process
%
%min_suprathresh_duration
%		- the time (in seconds) which the signal
%       must remain above threshold to be counted as as ripple; this guards
%       against short spikes in signal (e.g. noise) as being counted as
%       ripples. Set min_suprathreshold_duration to some small value, like
%       0.015 s.
%
%nstd		- the number of standard dev that ripple must be from mean to
%			be detextractconsensusected. Start with 2.
%
%options	'stdev', stdev   sets the size of the standard deviation used to
%				allow direct comparison across epochs
%       	'baseline', b   sets the size of the baseline used to
%				allow direct comparison across epochs
%           'maxpeakval, m	- ripples with maximal peaks above this value
%				are exluded.  Use this avoid detecting noise
%				events. Default 1000
%           'samethreshperday' - 0 or 1, default 0,
%               0 calculates baseline and threshold per epoch for each
%               tetrode
%               1 uses same threshold for all sessions on the same day on
%               the same tetrode.  Calculates this threshold based on the
%               baseline and stdev for all sessions (run and sleep) for
%               the entire day.    AS added 1-12-10
%
% Outputs:
%ripples 	- structue with various fields, including the following which
%			describe each ripple.
%	starttime - time of beginning of ripple
%	endtime	  - time of end of ripple
%	maxthresh - the largest threshold in stdev units at which this ripple
%			would still be detected.

disp(['Rip Konsensus processing ' animalprefix])
consvariablename = [eventname 'kons'];
stdev = 0;
baseline = 0;
maxpeakval = 2000;
samethreshperday = 0;
% define the standard deviation for the Gaussian smoother which we
% apply before thresholding (this reduces sensitivity to spurious
% flucutations in the ripple envelope)
smoothing_width = 0.004; % 4 ms

for option = 1:2:length(varargin)-1
    if isstr(varargin{option})
        switch(varargin{option})
            case 'stdev'
                stdev = varargin{option+1};
            case 'baseline'
                baseline = varargin{option+1};
            case 'maxpeakval'
                maxpeakval = varargin{option+1};
            case 'samethreshperday'
                samethreshperday = varargin{option+1};
            otherwise
                error(['Option ',varargin{option},' unknown.']);
        end
    else
        error('Options must be strings, followed by the variable');
    end
end

tetinfo = loaddatastruct(animaldir, animalprefix, 'tetinfo');
task = loaddatastruct(animaldir, animalprefix, 'task');

for d = days
    
    for ep = 1:length(task{d})
        
        if ep > length(tetinfo{d}) || ~isfield(task{d}{ep},'type') || strcmp(task{d}{ep}.type,'failed sleep')
            disp('task struct has one more epoch than in tetinfo (or aberrant .type) - disregarding this epoch')
            continue
        end
        
        for TF = 1:length(tetfilters)
            
            tetfilter = tetfilters{TF};
            
            % retrieve valid tetrodes to iterate through
            tetlist =  evaluatefilter(tetinfo{d}{ep},tetfilter)';
            
            if isempty(tetlist)
                disp(sprintf('no valid detecting tetrodes, d %d e %d',d,ep))
                ev.eventname = '';
                ev.nstd = [];
                ev.min_suprathresh_duration = [];
                ev.tetfilter = tetfilter;
                ev.tetlist = [];
                ev.starttime = [];
                ev.endtime = [];
                ev.maxthresh = [];
                eventscons{d}{ep}{TF} = ev;
                clear ev;
                continue
            end
            if length(tetlist) < 3
                disp(sprintf('day %d ep %d: less than 3 tets!',d,ep))
            end
            
            % load eeg data
            eventeeg = loadeegstruct(animaldir, animalprefix, eventeegname, d, ep, tetlist);
            
            %DR aug 2016 tmp change start time to double 
            if ~isa(eventeeg{d}{ep}{tetlist(1)}.starttime, 'double')
                eventeeg{d}{ep}{tetlist(1)}.starttime = double(eventeeg{d}{ep}{tetlist(1)}.starttime);
            end
            % obtain times vector from one of the tetrodes as a reference
            %DR Jan 2017... get the eeg times from an eeg file. 
            eegtimesvec_ref = geteegtimes(eventeeg{d}{ep}{tetlist(1)});
%             eegtimesvec_ref = 
            epoch_starttime = eegtimesvec_ref(1);
            epoch_endtime = eegtimesvec_ref(end);
            numsamples_refeeg = length(eventeeg{d}{ep}{tetlist(1)}.data(:,1));
            if numsamples_refeeg ~= length(eegtimesvec_ref)
                keyboard
            end
            
            % obtain the squared amplitudes from each tetrode
            sqamps = nan(length(tetlist),length(eegtimesvec_ref));  % initialize
            for tt = 1:length(tetlist)
                tet = tetlist(tt);
                data = double(eventeeg{d}{ep}{tet}.data(:,1))'; 
                % if there are brief periods of wild noise (filtered amplitude exceeds maxval),
                % set values to 0 as a patch fix and report
                wildnoise = (abs(data) > maxpeakval);
                if any(wildnoise)
                    ms_of_wildnoise = round(1000*sum(wildnoise)/1500);
                    if ms_of_wildnoise > 10000
                        % if more than 5 s of wild noise, don't report consensus events in this case -- abandon the epoch
                        ev.eventname = '';
                        ev.nstd = [];
                        ev.min_suprathresh_duration = [];
                        ev.tetfilter = tetfilter;
                        ev.tetlist = [];
                        ev.starttime = [];
                        ev.endtime = [];
                        ev.maxthresh = [];
                        disp(sprintf('anim %s day%dep%d tet %d: wild noise periods totalling %d millisec (if 1500 Hz) -- (!!) exceeds 10 s',...
                            animalprefix,d,ep,tet,ms_of_wildnoise))
                        disp('(!!!!!) excluding epoch because of this -- consider eliminating tet as detector!')
                        eventscons{d}{ep}{TF} = ev;
                        clear ev;                        
                        continue
                    else
                        disp(sprintf('anim %s day%dep%d tet %d: wild noise periods totalling %d millisec (if 1500 Hz) excluded in threshold calc',...
                            animalprefix,d,ep,tet,ms_of_wildnoise))
                        % simply set violating values to zero
                        data(wildnoise) = 0;
                    end
                end
                if length(data) == numsamples_refeeg
                    sqamps(tt,:) = data.^2;
                else
                    % if mismatch in # of samples for this tetrode (different DSPs often start at slightly different times),
                    % 'interpolate' (really just quick way of appending NaNs on the deficient end)
                    eegtimesvec = geteegtimes(eventeeg{d}{ep}{tet});
                    interpdata = interp1(eegtimesvec,data,eegtimesvec_ref,'nearest')';
                    sqamps(tt,:) = interpdata.^2;
                end
            end
            
            % basic values
            samprate = eventeeg{d}{ep}{tetlist(1)}.samprate;
            mindur = round(min_suprathresh_duration * samprate);             
            
            % individually smooth the squared amplitudes with a kernel,
                % then take square root
            kernel = gaussian(smoothing_width*samprate, ceil(8*smoothing_width*samprate));
            rms_amps = nan(size(sqamps));
            for tt2 = 1:length(tetlist)
                rms_amps(tt2,:) = realsqrt(smoothvect(sqamps(tt2,:), kernel));
            end
            
            % Now take the mean across sites
            powertrace = mean(rms_amps,1);
            
           
            
            % calculate mean and std of powertrace, to use in extractevents
            powertrace_values = powertrace(~isnan(powertrace));
            baseline = mean(powertrace_values);
            stdev = std(powertrace_values);
            thresh = baseline + nstd * stdev;
            
            if any(~isreal([baseline stdev]))
                error('these should be real valued..')
            end
            
            % optional: plot power and threshold
            if 0
                figure
                whitebg([0 0 0])
                plot(eegtimesvec_ref-eegtimesvec_ref(1),powertrace,'w')
                hold on
                plot([eegtimesvec_ref(1) eegtimesvec_ref(end)]-eegtimesvec_ref(1),[baseline baseline],'b');
                plot([eegtimesvec_ref(1) eegtimesvec_ref(end)]-eegtimesvec_ref(1),[thresh thresh],'r');
            end
            
            % extract the events if this is a valid trace
            if (thresh > 0) && any(find(powertrace<baseline))
                disp(sprintf('day %d epoch %d %s',d,ep, consvariablename))
                % some odd error within the extractevents .mex makes it lock up
                % sometimes if the powertrace is not doubled.. therefore down below I just
                % take the first half of the output
                tmpevent = extractevents([powertrace'; powertrace'], thresh, baseline, 0, mindur, 0)';
                if mod(size(tmpevent,1),2)==0
                    lastind = size(tmpevent,1)/2;
                else
                    lastind = (size(tmpevent,1)-1)/2;
                end
                startind = tmpevent(1:lastind,1);
                endind = tmpevent(1:lastind,2);
                % Assign the fields
                ev.eventname = eventname;
                ev.nstd = nstd;
                ev.min_suprathresh_duration = min_suprathresh_duration;
                ev.tetfilter = tetfilter;
                ev.tetlist = tetlist;
                ev.starttime = epoch_starttime + startind / samprate;
                ev.endtime = epoch_starttime + endind / samprate;
                ev.maxthresh = (tmpevent(1:lastind,9) - baseline) / stdev;
            else
                ev.eventname = '';
                ev.nstd = [];
                ev.min_suprathresh_duration = [];
                ev.tetfilter = '';
                ev.tetlist = [];
                ev.starttime = [];
                ev.endtime = [];
                ev.maxthresh = [];
            end
            
            if any(find(powertrace<baseline))==0
                warning(['No below baseline values in data.  Fields left blank, day ' num2str(d) 'epoch ' num2str(ep)])
            end

            ev.timerange = [epoch_starttime epoch_endtime];
            ev.samprate = samprate;
            ev.baseline = baseline;
            ev.std = stdev;
            
            % for troubleshooting purposes can toggle this -- will make files
            % large though (and could make kk_getconstimes use a ton of memory)
            if 1 %DR saving out the powertrace
                ev.eegtimesvec_ref = eegtimesvec_ref;
                ev.powertrace = powertrace;
            end
            
            %event
            clear eventeeg
            eventskons{d}{ep}{TF} = ev;
            clear ev;
            
        end
        
    end
    

if exist('eventskons','var')
eval([consvariablename ' = eventskons;']);
save(sprintf('%s/%s%skons%02d.mat', animaldir, animalprefix, eventname,d),consvariablename);
cd(animaldir)    
clear eventskons;
else
   disp(sprintf('consensus not saved for %s %s, day %d',animalprefix,eventname,d))
end
    
end

end
