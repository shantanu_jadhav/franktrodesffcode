function Binary2FF_POS(animalID, srcdir, destdir, sessionNum, date, varargin)

%Demetris Roumis July 2016
%This function saves RAWPOS and POS data in the Filter Framework v1 format.
%It is assumed that there is a directory of binary files in the form of:
%  <username>/<animalname>/extracted/<date (%YYYY%MM%DD)>/<extracted binary files>

%animalID -- a string identifying the animal's id (appended to the beginning of the files).. ex: 'D10'
%sessionNum -- day.. ex: '4'
%srcdir -- preprocessed binary directory.. ex: /mnt/data19/droumis/D10/preprocessed/20160512/
%destdir -- the directory where the processed files should be saved for the animal...
%ex: /mnt/data19/droumis/D10/filterframework/

%OPTIONS -- optional input in the form of: 'option', value, 'option', value
%           optional arguments are:
%           DIODEPOS -- 0 uses back diode for pos, 1 uses front diode for
%               pos, and values in between use the proportionate distance
%               between the two diodes. (default 0.5, ie, average of diodes)
%           CMPERPIX -- size of each pixel in centimeters (must be specified)
%           POSFILT -- filter to use for pos smoothing when computing
%               velocity (default gaussian(30*0.5, 60))
%           REVERSEX -- mirror image pos info along x axis (default 0)
%           REVERSEY -- mirror image pos info along y axis (default 0)
%           SOURCE -- which pos source to use? can be
%                        'online','manual1',etc.  default online

% set default values for the optional arguments for POS processing
diodepos = 0.5; %centroid
cmperpix = NaN;
posfilt = gaussian(30*0.5, 60);
reversex = 0;   % NOTE if you  actually want to use reverse x or y, need to update cam frame size in posinterp
reversey = 0;
maxdevpoints = 30;
maxv = 300;
posSource = 'online';
useNearestNeighbor = 0; %DR pos realign

% process varargin and overwrite default values
if (~isempty(varargin))
    assign(varargin{:});
end
%% Look for existing rawpos and pos
cd(destdir);
if ~isempty(dir([animalID 'rawpos' num2str(sprintf('%02d', sessionNum)) '.mat']))
    disp('rawpos file detected.. will try to append to existing structure')
    load([animalID 'rawpos' num2str(sprintf('%02d', sessionNum)) '.mat']) %loads as pos var
else
    disp('no rawpos file detected.. proceeding to create one from scratch')
end
if ~isempty(dir([animalID 'pos' num2str(sprintf('%02d', sessionNum)) '.mat']))
    disp('pos file detected.. will try to append to existing structure')
    load([animalID 'pos' num2str(sprintf('%02d', sessionNum)) '.mat']) %loads as pos var
else
    disp('no pos file detected.. proceeding to create one from scratch')
end
%% Create rawpos file
cd(srcdir);
rawposepochDirs = dir('*.pos');
rawposepochDirs = arrayfun(@(x) x.name,rawposepochDirs,'UniformOutput', false);
rawposepochDirs = sort(rawposepochDirs); %this should be safe if the file naming is consistent with '<date>_<animal>_<2digitepoch>.pos'
disp(sprintf('Processing %d binary pos files...', length(rawposepochDirs)));
tic
for epoch = 1:length(rawposepochDirs)
    cd(srcdir); %necessary for every loop after the first
    cd(rawposepochDirs{epoch}); %go into current epoch pos dir
    posString = sprintf('*.pos_%s*',posSource);
    rawposntrodefiles = dir(posString);
    if isempty(rawposntrodefiles) %if there's no position tracking file with the posSource suffix, look in the raw folder
        animalinfo = animaldef(animalID);
        rawdir = animalinfo{4};
        disp(sprintf('couldn''t find %s pos tracking in preprocessing folder... taking a look in the raw dir instead', posSource))
        rawposDateDir = sprintf('%s%d/', rawdir, date);
        posString = sprintf('%s%d_%s_%02d*.pos_%s*', rawposDateDir, date, animalID, epoch, posSource);
        rawposntrodefiles = dir(posString);
        if isempty(rawposntrodefiles)
            disp(sprintf(':''( couldn''t find %s pos tracking in the raw dir either... aborting', posSource))
            return
        else
            preproccopy = 'y';
%             preproccopy = input(sprintf('raw success!.. Try to put a copy of %s in the preprocessed folder (y/n)?', rawposntrodefiles.name),'s');
            %this got annoying real fast.. default is now to make a copy
            if strcmp(preproccopy,'y')
                system(sprintf('scp %s%s %s%s/%s',rawposDateDir, rawposntrodefiles.name, srcdir,rawposepochDirs{epoch}, rawposntrodefiles.name));
            end
        tmpData = [];
        tmpData = readTrodesExtractedDataFile(sprintf('%s%s',rawposDateDir, rawposntrodefiles.name));    
        end
        
    else
        tmpData = [];
        try
        tmpData = readTrodesExtractedDataFile(rawposntrodefiles.name);
        catch
            disp('breakpoint')
        end
    end
   
    % combine the timestamps x1 y1 x2 y2 into a double array, in this order
    fieldsstruct = arrayfun(@(x) x.name,tmpData.fields,'UniformOutput', false); %get order of datatypes in array
    postrackingtimestamps = (double(tmpData.fields(find(strcmp([fieldsstruct(:)], 'time'))).data))./tmpData.clockrate; %put timestamps into seconds
    x1posData = double(tmpData.fields(find(strcmp([fieldsstruct(:)], 'xloc'))).data);
    y1posData = double(tmpData.fields(find(strcmp([fieldsstruct(:)], 'yloc'))).data);
    x2posData = double(tmpData.fields(find(strcmp([fieldsstruct(:)], 'xloc2'))).data);
    y2posData = double(tmpData.fields(find(strcmp([fieldsstruct(:)], 'yloc2'))).data);
    
    %load pos_timestamps file (offset-adjusted) to use as timestamps instead
    if ~isempty(dir('*.pos_timestamps.adj*'));
        postimefileADJUSTED = dir('*.pos_timestamps.adj*'); %there can be only one
        postimestamps = readCameraModuleTimeStamps(postimefileADJUSTED.name);
        
        %WRONGO!!
        %         postrackingtimestamps = (double(tmpData.fields(find(strcmp([fieldsstruct(:)], 'time'))).data))./tmpData.clockrate; %put timestamps into seconds
        %         if length(postimefilestampsADJUSTED) == length(postrackingtimestamps);
        %             postimestamps = postimefilestampsADJUSTED;
        %
        %         else % the timestamps from the position tracking likely won't be the same length as the timestamps from the timestampsfiles.. it's probably missing a frame or two on the ends
        %             % so use the UNadjustedtimestamps to map the position tracking timestamps onto the adjustedtimestamps
        %             disp('number of adjusted timestamps and tracking data points do not match, using unadjusted timestamps to map postracking times onto adjusted timestamps');
        %             postimestamps =  postimefilestampsADJUSTED(lookup(postrackingtimestamps,postimefilestamps));
        %         end
        
    elseif ~isempty(dir('*.pos_timestamps.dat*'));
        disp('adjusted timestamps file not detected, press any key to continue with unadjusted timestamps or ctrl-c to abort');
        pause
        postimefile = dir('*.pos_timestamps.dat');
        postimestamps = readCameraModuleTimeStamps(postimefile.name);
    else
        disp('timestamps file not detected, press any key to continue with position tracking timestamps or ctrl-c to abort');
        pause
        postimestamps = postrackingtimestamps;
    end
    
    % realign timestamps based on dios and camlogs, if available    
    if useNearestNeighbor
        [newTimes newTimesInds] = DR_pos_realign(srcdir, destdir, animalID, sessionNum, epoch, rawposepochDirs{epoch}, postimestamps);
    else
        disp('need to implement AG method outputting of nan ind struct for repeat regions before this will work')
        pause
        newTimes = AG_pos_realign(srcdir, destdir, animalID, sessionNum, epoch, rawposepochDirs{epoch}, postimestamps);
    end
    cd(srcdir);
    cd(rawposepochDirs{epoch});
    postimefile = dir('*.pos_timestamps.dat');
    postimestampsUNadj = readCameraModuleTimeStamps(postimefile.name);
    
    %lookup map postrackingtimestamps to postimestampsUNadj to push pos data to realigned and adjusted timestamps.. hack fix because offline tracking grabs unadjusted timestamps and cuts off first and last frame >:|
    trackingmap = lookup(postrackingtimestamps, postimestampsUNadj); %get index map between unadjusted timestamps and the timestamps in the pos tracking file  
    rawposDatatmp = [x1posData y1posData x2posData y2posData]; %collect the pos tracking data
    rawposData = zeros(length(postimestampsUNadj),length(rawposDatatmp(1,:))); %initilize
    rawposData(trackingmap,:) = rawposDatatmp; % plase pos tracking data into new struct based on mapping of tracking to unadjusted timestamps
    rawposData = rawposData(newTimesInds,:); %remove any nan'ed repeats found in pos_realign
    rawposData = [newTimes rawposData]; %add timestamps to pos tracking data
    rawpos{sessionNum}{epoch}.data = rawposData; 
    rawpos{sessionNum}{epoch}.fields = ['timestamp x1 y1 x2 y2'];
    rawpos{sessionNum}{epoch}.descript = { };
    rawpos{sessionNum}{epoch}.threshold = tmpData.threshold;
    rawpos{sessionNum}{epoch}.dark = tmpData.dark;
    rawpos{sessionNum}{epoch}.clockrate = tmpData.clockrate;
    
    % -------------create pos files--------------------------------
    if isfinite(cmperpix) & (length(cmperpix) < length(rawposepochDirs)); %if all epochs don't have their own cmperpix
        cmperpix = repmat(cmperpix(1),1,length(rawposepochDirs)); %if only 1 value for cmperpix, copy to length of epochs
    end
    
    if ~isfinite(cmperpix)
        error('cmperpix must be specified.. cmperpixer.m can help');
    end
    
    if ~isempty(rawpos{sessionNum}{epoch}.data)
        
        %first, realign pos timestamps based on DIOs
       disp('smoothing pos, calculating velocity etc...')
        pos{sessionNum}{epoch} = posinterp(rawpos{sessionNum}{epoch}, 'diode', diodepos,...
            'maxv', 300, 'maxdevpoints', 30, 'reversex', reversex, 'reversey', reversey);
        % multiply pos data by cmperpix to convert from pixel units to cm
        pos{sessionNum}{epoch}.cmperpixel = cmperpix(epoch);
        pos{sessionNum}{epoch}.data(:,2:3) = pos{sessionNum}{epoch}.data(:,2:3)*cmperpix(epoch);
        % calculate and add velocity to the pos struct:: pos = [time x y dir vel-Gauss]
        pos{sessionNum}{epoch} = addvelocity(pos{sessionNum}{epoch}, posfilt); %Gaussian filter
        % calculate the smoothed direction, velocity, and position:: smoothpos = [time X-Loess Y-Loess Dir-Loess Vel-Loess]
        Loesssmoothpos = sj_estimate_position(rawpos{sessionNum}{epoch},'centimeters_per_pixel',cmperpix(epoch)); %Loess filter
        mappostimeind = lookup(Loesssmoothpos{1}.data(:,1),pos{sessionNum}{epoch}.data(:,1)); %map pos times to Loess pos indicies..aka find closest time
        pos{sessionNum}{epoch}.data(mappostimeind,[1 6 7 8]) = Loesssmoothpos{1}.data(mappostimeind,[1 2 3 4]);
        % pos = [time x y dir vel-Gauss X-Loess Y-Loess Dir-Loess Vel-Gauss-Loess]
        pos{sessionNum}{epoch}.data(mappostimeind,9) = sj_addnfiltvelocity(Loesssmoothpos{1}.data(:,2:3), Loesssmoothpos{1}.data(:,1)); %gaussian filter
        %------------------------%
        %{sessionNum}{rawposepoch}(:,[9]) = smoothpos{1}.data(:,[5]); %x-sm y-sm dir-sm
        %FYI::: SJ didn't use  smoothed vel from estimate_position.m (Loess) ; instead used sj_addnfiltvelocity (Gaussian with chopped tail)
        %SJ said he thinks this came from Caleb/Maggie claiming it works better than Loess... and because that's that's kernal that is used
        %for online velocity smoothing.... That's why it's reordered here into col 9
        %------------------------%
       pos{sessionNum}{epoch}.fields = 'time x y dir vel x-sm y-sm dir-sm vel-sm'; %vel is actually speed, not vel, just sayin :)
        
    else
        disp(sprintf('pos and rawpos not saved for Day%d epoch%d', sessionNum, epoch))
        pos{sessionNum}{epoch}.data = [];
        pos{sessionNum}{epoch}.cmperpixel = cmperpix(epoch);
    end
end
save([destdir animalID,'rawpos',num2str(sprintf('%02d',sessionNum)),'.mat'],'-v6','rawpos'); %one rawpos file per day
save([destdir animalID,'pos',num2str(sprintf('%02d',sessionNum)),'.mat'],'-v6','pos'); %one pos file per day
FFv1filetime = toc; disp([sprintf('%.02f',FFv1filetime) ' seconds to make POS files'])
