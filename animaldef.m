function animalinfo = animaldef(animalname)

switch lower(animalname)

    
    
    
    %Anna's animals
    case 'algernon'
        animalinfo = {'algernon','/data45/algernon/filterframework/','algernon','/data45/algernon/raw/','/data45/algernon/preprocessing/'};
    case 'surprise'
        animalinfo = {'surprise','/data45/surprise/filterframework/','surprise','/data45/surprise/raw/','/data45/surprise/preprocessing/'};

%     
%     %Demetris' animals
%     case 'd13'
%         animalinfo = {'D13', '/mnt/data46/droumis/D13/filterframework/', 'D13', '/mnt/data46/droumis/D13/raw/', '/mnt/data46/droumis/D13/preprocessing/'};
%     case 'd13'
%         animalinfo = {'D13', '/mnt/data19/droumis/D13/filterframework/', 'D13', '/mnt/data19/droumis/D13/raw/', '/mnt/data19/droumis/D13/preprocessing/'};
%     case 't4g'
%         animalinfo = {'T4G', '/mnt/data19/droumis/T4G/filterframework/', 'T4G', '/mnt/data19/droumis/T4G/raw/', '/mnt/data19/droumis/T4G/preprocessing/'};
%     case 'd12'
%         animalinfo = {'D12', '/mnt/data19/droumis/D12/filterframework/', 'D12', '/mnt/data19/droumis/D12/raw/', '/mnt/data19/droumis/D12/preprocessing/'};
%     case 'd10'
%         animalinfo = {'D10', '/mnt/data19/droumis/D10/filterframework/', 'D10', '/mnt/data19/droumis/D10/raw/', '/mnt/data19/droumis/D10/preprocessing/'};  
%     case 'd09'
%         animalinfo = {'D09', '/mnt/data19/droumis/D09/filterframework/', 'D09', '/mnt/data19/droumis/D09/raw/', '/mnt/data19/droumis/D09/preprocessing/'};
%     case 'd05'
%         animalinfo = {'D05', '/mnt/data19/droumis/D05/filterframework/', 'D05', '/mnt/data19/droumis/D05/raw/', '/mnt/data19/droumis/D05/preprocessing/'};
%     case 't15'
%         animalinfo = {'T15', '/mnt/data19/droumis/T15/', 't15'};

    %Demetris' animals
    case 'jz1'
        animalinfo = {'JZ1', '/typhoon/droumis/JZ1/filterframework/', 'JZ1', '/typhoon/droumis/JZ1/raw/', '/typhoon/droumis/JZ1/preprocessing/'};
    case 'd13'
        animalinfo = {'D13', '/typhoon/droumis/D13/filterframework/', 'D13', '/typhoon/droumis/D13/raw/', '/typhoon/droumis/D13/preprocessing/'};
%     case 't4g'
%         animalinfo = {'T4G', '/typhoon/droumis/T4G/filterframework/', 'T4G', '/typhoon/droumis/T4G/raw/', '/typhoon/droumis/T4G/preprocessing/'};
    case 'd12'
        animalinfo = {'D12', '/typhoon/droumis/D12/filterframework/', 'D12', '/typhoon/droumis/D12/raw/', '/typhoon/droumis/D12/preprocessing/'};
    case 'd10'
        animalinfo = {'D10', '/typhoon/droumis/D10/filterframework/', 'D10', '/typhoon/droumis/D10/raw/', '/typhoon/droumis/D10/preprocessing/'};  
    case 'd09'
        animalinfo = {'D09', '/typhoon/droumis/D09/filterframework/', 'D09', '/typhoon/droumis/D09/raw/', '/typhoon/droumis/D09/preprocessing/'};
    case 'd05'
        animalinfo = {'D05', '/typhoon/droumis/D05/filterframework/', 'D05', '/typhoon/droumis/D05/raw/', '/typhoon/droumis/D05/preprocessing/'};
    case 't15'
        animalinfo = {'T15', '/typhoon/droumis/T15/', 't15'};        
    
    %Mari's animals
    case 'apollo'
        animalinfo = {'Apollo','/opt/data40/mari/Apo/','apo'};
    case 'brody'
        animalinfo = {'Brody','/opt/data40/mari/Bro/','bro'};
    
    % Annabelle's animals
    case 'arnold'
        animalinfo = {'Arnold', '/datatmp/kkay/Arn/', 'arn'};
    case 'barack'
        animalinfo = {'Barack', '/data12/kkay/Bar/', 'bar'};
    case 'calvin'
        animalinfo = {'Calvin', '/data12/kkay/Cal/', 'cal'};
    case 'dwight'
        animalinfo = {'Dwight', '/data12/kkay/Dwi/', 'dwi'};
        
    % Kenny's animals
    case 'chapati'
        animalinfo = {'Chapati','/opt/data40/mari/Cha/','cha'};
    case 'dave'
        animalinfo = {'Dave','/data12/kkay/Dav/','dav'};
    case 'government'
        animalinfo = {'Government','/opt/data40/mari/Gov/','gov'};
        
    % Kenny & Mari's animals    
    case 'egypt'
        animalinfo = {'Egypt','/opt/data40/mari/Egy/','egy'};
    case 'higgs'
        animalinfo = {'Higgs','/opt/data40/mari/Hig/','hig'};
        

    case 'frank'
        animalinfo = {'Frank', '/data12/kkay/Fra/', 'fra'};
    case 'bond'
        animalinfo = {'Bond', '/data12/kkay/Bon/', 'bon'};
        
    % Maggie's animals
    case 'corriander'
        animalinfo = {'Corriander','/data12/kkay/Cor/','Cor'};
        
    %Jai's animals
    case 'r1'
        animalinfo = {'R1','/opt/data40/mari/FromJai/R1_/','R1'};
        

    % Mattias' animals

    case 'miles'
        animalinfo = {'Miles', '/data/mkarlsso/Mil/', 'mil'};
    case 'nine'
        animalinfo = {'Nine', '/data/mkarlsso/Nin/', 'nin'};
    case 'ten'
        animalinfo = {'Ten', '/data/mkarlsso/Ten/', 'ten'};
    case 'dudley'
        animalinfo = {'Dudley', '/data/mkarlsso/Dud/', 'dud'};
    case 'alex'
        animalinfo = {'Alex', '/data/mkarlsso/Ale/', 'ale'};
    case 'conley'
        animalinfo = {'Conley', '/data/mkarlsso/Con/', 'con'};

    case 'five'
        animalinfo = {'Five', '/data13/mcarr/Fiv/', 'Fiv'};

    % Ana's animals
    case 'm01'
        animalinfo = {'m01', '/data/ana/M01/', 'm01'};
    case 'm02'
        animalinfo = {'m02', '/data/ana/M02/', 'm02'};
    otherwise
        error(['Animal ',animalname, ' not defined.']);
end
