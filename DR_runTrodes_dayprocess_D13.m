
%sends parameters to Trodes_dayprocess
%Create one of these scripts per animal

%-------------Animal Info-------------------
animal = 'D13';
% dates = [20161006 20161007 20161008 20161009 20161010 20161011 20161012 20161013 20161015 ...
%     20161016 20161017 20161019 20161020 20161021 20161022 20161024]; % input a single day/session by it's 8 digit date YYYYMMDD e.g. 20160512
% sessionNums = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15]; % Day(s)
dates = [20161006 20161007 20161008 20161009 20161010 20161011 20161012];
sessionNums = [1 2 3 4 5 6 7]; % Day(s)
sleepcmperpix = [0.06122];
runcmperpix = [0.06545];
nTrodeareas = [{'ca1'} {[16:21, 23:30]} ; {'mec'} {[1:4, 6, 8, 9, 12:15]} ; {'por'} {[5 7 10]}]; % [ {area1} {[tetrodes]} ; {area2} {tetrodes} ... ]
refNTrodes = [{'ca1'} {[22]} ; {'mec'} {[11]} ; {'por'} {[11]}]; %gets used to reference LFP if useconfig == 0;

%List out the epoch types ordered for each day
epochtypesorderedCombined{1} = {'sleep', 'wtrack', 'sleep', 'wtrack', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{2} = {'sleep', 'wtrack', 'sleep', 'wtrack', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{3} = {'sleep', 'wtrack', 'sleep', 'wtrack', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{4} = {'sleep', 'wtrack', 'sleep', 'wtrack', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{5} = {'sleep', 'wtrack', 'sleep', 'wtrack', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{6} = {'sleep', 'wtrack', 'sleep', 'wtrack', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{7} = {'sleep', 'wtrack', 'sleep', 'wtrack', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{8} = {'sleep', 'wtrack', 'sleep', 'wtrackrotated', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{9} = {'sleep', 'wtrack', 'sleep', 'wtrackrotated', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{10} = {'sleep', 'wtrack', 'sleep', 'wtrackrotated', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{11} = {'sleep', 'wtrackdoubleright', 'sleep', 'wtrackdoubleright', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{12} = {'sleep', 'wtrackdoubleright', 'sleep', 'wtrackdoubleright', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{13} = {'sleep', 'wtrackdoubleright', 'sleep', 'wtrackdoubleright', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{14} = {'sleep', 'wtrackdoubleright', 'sleep', 'wtrackdoubleleft', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
epochtypesorderedCombined{15} = {'sleep', 'wtrackdoubleright', 'sleep', 'wtrackdoubleright', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};

%Order of overall eptype occurance
epochtagsordered = [{'environment', 'sleep', 'type', 'sleep'};...
    {'environment', 'wtrack', 'type', 'run'};...
    {'environment', 'openfield', 'type', 'run'};...
    {'environment', 'wtrackrotated', 'type', 'run'};...
    {'environment', 'wtrackdoubleright', 'type', 'run'};...
    {'environment', 'wtrackdoubleleft', 'type', 'run'}];...
   
%-------------Params------------------
adjusttimestamps = 0; % step 1 ;  %Run this once before processing anything else..
makeFFLFP = 0; % step 2
makereferencedFFLFP = 0; useconfig = 0;  % DR: for D13.. set useconfig = 0.. 
makeFFDIO = 0; % step 4;
makeFFPOS = 0; % step 5;
updatetaskstruct = 0; % step 6
updatetetcellstructs = 0; % step 7
makeFFLINPOS = 0; % step 8
makeRippleFilteredLFP = 0; % step 9;
extractRipplesKonsensus = 1; % step 10; new consensus method..KK has other options that alter smooth,square,mean order... see KK's extraction.m

%_____Don't Run these yet:________
extractRipples = 0; %old method ripple detection. DONT RUN THIS
makeFFspikes = 0; % code isn't done yet. DONT RUN THIS

% ___________________General Params (probably won't need to change these)___________________
% posSource = 'manual1';
posSource = 'online';
taskcoordinateprogram = 'sj_getcoord_wtrack'; %'sj_getcoord_wtrack' for W track..'doc' getcoord_wtrack for click order
overwriteTrackNodes = 0; % set this to 1 if you want to overwrite the user specified nodes of the wtrack.. i.e. reruns createtaskstruct 
dayepochlist = []; %[day epoch; day epoch;...] eventually just grab this from the .trodescomments
% dayepochlist = [2 1; 2 3; 2 5; 2 7];
% dayepochlist = [2 6];
% epochs2tag = [2 4];
% epochs2tag = [1 3 5 7];
epochs2tag = [];
% epochtags = {'environment', 'wtrack', 'type', 'run'}; % {property1, value1, property2, value2, …} e.g. {'environment', 'wtrack', 'type', 'run'}epochtags = {'environment', 'wtrack', 'type', 'run'};
% epochtags = {'environment', 'sleep', 'type', 'sleep'};% {property1, value1, property2, value2, …} e.g. {'environment', 'wtrack', 'type', 'run'}
% epochs2tag = [6 8];
epochtags = [];%{'environment', 'openfield', 'type', 'run'};
% nTrodeareas = [{'ca1'} {[1, 16:30]} ; {'mec'} {[3:8 11:14]} ; {'por'} {[10 15]} ; {'v2l'} {[9]}; {'ref'} {[2 18]}]; % [ {area1} {[tetrodes]} ; {area2} {tetrodes} ... ]
RIPmindur = 0.015; % time (in seconds) which the signal must remain above threshold to be counted as as ripple (start with 0.015)
RIPnstd = 2; % > # std ripples (start with 2)
OLDRIPtetrodes = [1 16 17 19:30]; % -1 to specify all tetrodes. eventually grab this from tetinfo struct based on area
consensusRIParea = {'ca1', 'mec', 'por'}; %define source region of ripples.. saved structure will contain this string
useNearestNeighbor = 1; %NN method current DR default.. AG set this to 0 for your data

% ___________________ Debugging ___________________
checkforcorrupteddata = 0;
checknumCommentfiles = 0; %not necessary anymore.. we aren't using the coments files


%% run Trodes_dayprocess with the vars above.. loop over dates/sessionNums
tic
for i = 1:length(dates);
    date = dates(i);
    sessionNum = sessionNums(i);
    epochtypesordered = epochtypesorderedCombined{i};
    cmperpix = repmat([sleepcmperpix runcmperpix],1,ceil(length(epochtypesordered)/2));
    cmperpix = cmperpix(1:length(epochtypesordered));
    
    %run dayprocess
    Trodes_dayprocess(animal, date, sessionNum, 'adjusttimestamps',adjusttimestamps, 'makeFFLFP', makeFFLFP,...
        'makereferencedFFLFP',makereferencedFFLFP, 'useconfig', useconfig, 'makeFFDIO', makeFFDIO, 'makeFFPOS', makeFFPOS, 'makeFFLINPOS', makeFFLINPOS, ...
        'makeFFspikes', makeFFspikes, 'updatetaskstruct', updatetaskstruct, 'updatetetcellstructs', updatetetcellstructs, ...
        'makeRippleFilteredLFP', makeRippleFilteredLFP, 'extractRipples', extractRipples, 'extractRipplesKonsensus', extractRipplesKonsensus, ...
        'cmperpix', cmperpix, 'posSource', posSource, 'taskcoordinateprogram', taskcoordinateprogram, 'dayepochlist', dayepochlist, 'epochs2tag', epochs2tag, ...
        'epochtags', epochtags, 'nTrodeareas', nTrodeareas, 'RIPmindur', RIPmindur, 'RIPnstd', RIPnstd, 'OLDRIPtetrodes', OLDRIPtetrodes, ...
        'consensusRIParea', consensusRIParea, 'checkforcorrupteddata', checkforcorrupteddata, 'checknumCommentfiles', checknumCommentfiles,...
        'useNearestNeighbor',useNearestNeighbor, 'epochtagsordered', epochtagsordered,'epochtypesordered', epochtypesordered, 'overwriteTrackNodes', overwriteTrackNodes,...
        'refNTrodes', refNTrodes);
end

timerTrodes_dayprocess = toc;
disp(sprintf('nextcoffeebreak = %0.0f seconds',timerTrodes_dayprocess));