function FF_LFPreferencer(animalID, rawdaydir,FFanimdir, sessionNum, varargin) 

eegdir = [FFanimdir 'EEG/'];

% AKG Aug2016
% uses lfp ref chan listed in eeggnd file or config file, if provided

%default is to use config to identify ref chan
useconfig = 1;
if (~isempty(varargin))
    assign(varargin{:});
end

if useconfig
    try   %there is probably a trodesconf file in each raw day folder
        configfile = sprintf('%s%s.trodesconf',rawdaydir,rawdaydir(end-8:end-1));
        config = readTrodesFileConfig(configfile);
        conforder = arrayfun(@(x) x.id,config.nTrodes,'UniformOutput', false); 
    catch
        try % otherwise, there should be one in the parent raw dir
            configfile = dir(sprintf('%s*.trodesconf',rawdaydir(1:end-9)));
            config = readTrodesFileConfig(configfile(1).name);
            conforder = arrayfun(@(x) x.id,config.nTrodes,'UniformOutput', false); 
        catch
            error('no config file found')
        end     
    end
end
        
cd(eegdir)
eeggndfiles = dir(sprintf('*eeggnd%02d*',sessionNum));

for lfpntrodeNum = 1:length(eeggndfiles)
    load(eeggndfiles(lfpntrodeNum).name)
    %xtract ep and tet info from filename
    dashes = strfind(eeggndfiles(1).name,'-');
    ep = str2num(eeggndfiles(lfpntrodeNum).name(dashes(1)+1:dashes(2)-1));
    tet = str2num(eeggndfiles(lfpntrodeNum).name(dashes(2)+1:end-4));
    %identify ref chan
    if useconfig
            % since only a single channel gets saved per tet, the chan you wish to use must have been the one that got saved as lfp during extraction
            reftet = str2num(config.nTrodes(find(strcmp(conforder,num2str(tet)))).refNTrode);
    else
        reftet = eeggnd{sessionNum}{ep}{tet}.refNTrode; %will exist eventually
    end
    refeeg = load(sprintf('%seeggnd%02d-%d-%02d.mat',animalID,sessionNum,ep,reftet));
        eeg = [];
        %do the actual subtraction of all non-nan values... i.e. retain nan's
        eeg{sessionNum}{ep}{tet}.data = nan(length(eeggnd{sessionNum}{ep}{tet}.data),1); % nanitialize
        inds2use = find(~isnan(eeggnd{sessionNum}{ep}{tet}.data));
        eeg{sessionNum}{ep}{tet}.data(inds2use) = eeggnd{sessionNum}{ep}{tet}.data(inds2use)-refeeg.eeggnd{sessionNum}{ep}{reftet}.data(inds2use);
        % copy everything else over from eeggnd
        eeg{sessionNum}{ep}{tet}.descript = eeggnd{sessionNum}{ep}{tet}.descript;
        eeg{sessionNum}{ep}{tet}.samprate = eeggnd{sessionNum}{ep}{tet}.samprate; %mattias will implement this as an export arg in the future
        eeg{sessionNum}{ep}{tet}.clockrate = eeggnd{sessionNum}{ep}{tet}.clockrate;
        eeg{sessionNum}{ep}{tet}.starttime = eeggnd{sessionNum}{ep}{tet}.starttime;
        eeg{sessionNum}{ep}{tet}.endtime = eeggnd{sessionNum}{ep}{tet}.endtime;        
        eeg{sessionNum}{ep}{tet}.data_voltage_scaled = eeggnd{sessionNum}{ep}{tet}.data_voltage_scaled;
        eeg{sessionNum}{ep}{tet}.nTrode = eeggnd{sessionNum}{ep}{tet}.nTrode;
        eeg{sessionNum}{ep}{tet}.nTrodeChannel = eeggnd{sessionNum}{ep}{tet}.nTrodeChannel;  %1-based
        eeg{sessionNum}{ep}{tet}.low_pass_filter = eeggnd{sessionNum}{ep}{tet}.low_pass_filter;
        eeg{sessionNum}{ep}{tet}.voltage_scaling = eeggnd{sessionNum}{ep}{tet}.voltage_scaling;
        % update referencing status
        eeg{sessionNum}{ep}{tet}.referenced = 1;
        eeg{sessionNum}{ep}{tet}.refnTrode = reftet;

        save(sprintf('%s%seeg%02d-%d-%02d.mat',eegdir,animalID,sessionNum,ep,tet),'-v6','eeg');
        disp(sprintf('referenced and saved %seeg%02d-%d-%02d.mat',animalID,sessionNum,ep,tet))
end

    