%function header = readtrodesexportheader(filehandle)
%returns the header from the file and leaves the file pointer at the end of it
function header = readtrodesexportheader(filehandle)
    header = {};
    headerind = 1;
    done = 0;
    while ~done
	headerline = fgetl(filehandle);
    	header{headerind} = headerline;
	headerind = headerind+1;
	if (strcmp(headerline, '<End settings>') == 1)
	    done = 1;
	end
    end
end


